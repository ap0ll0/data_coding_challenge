#!/usr/bin/env python3
import math

import json
import os
import pathlib
import sys
import time
from typing import Optional, TextIO

try:
    from loguru import logger
    level_val = os.environ.get('LOGURU_LEVEL', 'INFO')
    logger.configure(handlers=[{"sink": sys.stdout, "level": level_val}])

except ImportError:
    import logging
    logger = logging.getLogger(__name__)

import sys


class IndexedReader:
    """A helper class to speed-up reading data from large files (up to 1TB).

    This class uses .seek() on a file object handle, indexes and partial indexes saved in .idx file to speed-up access.
    There's no safety handling if you use a wrong index file, or change the input file without changing the index file.
    """

    def __init__(self, index_filename: str):
        # Each input file will have index file with .idx suffix in the same directory.
        self.index_file = pathlib.Path(index_filename)
        # Contents of index_file; e.g. {"1": 12} - .seek(12) to get to line 1.
        self.index_data = {}

    def read_seq(self, f_handle: TextIO, idx: int, offset: int, partial_index: int = 0) -> str:
        """Enumerate through lines to find line index.

        :param f_handle: File object handle to read data from.
        :param idx: Integer line index we're looking for; e.g. 100 for 100th line.
        :param offset: Number of bytes we have to read to get to line idx we passed.
        :param partial_index: Number of lines we have to skip if we don't have offset for line index.

        WARNING: there's no error handling if the index isn't found (file smaller than line index we're looking for).
        """
        for i, line in enumerate(f_handle):
            if i + partial_index == idx:
                self.write_index(idx, offset)
                return line
            offset += len(line) + 1

        raise RuntimeError(f'User error: file exhausted - no {idx}-th line')

    def get_random_line(self, f_handle: TextIO, idx: int) -> str:
        offset, partial_index = self.read_index(idx)

        if offset and partial_index:
            f_handle.seek(offset)
            result = self.read_seq(f_handle, idx, offset, partial_index).strip()
            return result

        elif offset:
            f_handle.seek(offset)
            result = f_handle.readline().strip()
            return result
        else:
            result = self.read_seq(f_handle, idx, offset).strip()
            return result

    def read_index(self, idx: int) -> (int, Optional[int]):
        partial_index = None
        if self.index_file.is_file():
            self.index_data = json.loads(self.index_file.read_text())
            try:
                # Exact match, cached index.
                return int(self.index_data[str(idx)]), partial_index
            except (KeyError, ValueError, TypeError):
                try:
                    # Find the closest index.
                    for new_idx in sorted(int(x) for x in self.index_data.keys()):
                        if new_idx < idx:
                            partial_index = new_idx
                            # print('closest match, ', partial_index)
                        else:
                            break
                    if partial_index is not None:
                        return self.index_data[str(partial_index)], partial_index
                except StopIteration:
                    pass

        return 0, partial_index

    def write_index(self, idx: int, offset: int):
        # index_data has to be already populated, otherwise we lose data
        self.index_data[idx] = offset
        self.index_file.write_text(json.dumps(self.index_data))


def main(preopt=True):
    """Main entry point for cli interface."""
    file_name = sys.argv[1]
    idx = int(sys.argv[2])
    index_file = pathlib.Path(f'{file_name}.idx')

    start = time.time()
    if preopt and not index_file.exists():
        f_size = os.path.getsize(file_name)
        index_count = 8000
        approx_lines = math.floor(f_size / 1e3)
        while index_count >= approx_lines:
            index_count /= 10
        index_count = 1 if index_count < 1 else int(index_count)

        step = math.ceil((approx_lines/index_count) -1)
        logger.info(f'Pre-building indexes: count={index_count} step={step}')
        helper = IndexedReader(str(index_file))
        for i in range(1, index_count - 1, step):
            with open(file_name, 'r', encoding='utf8') as f_handle:
                helper.get_random_line(f_handle, i * step)

    helper = IndexedReader(str(index_file))
    with open(file_name, 'r', encoding='utf8') as f_handle:
        line = helper.get_random_line(f_handle, idx)
    print(line)

    # Print how long it took to read the line for simple performance profiling.
    logger.debug(f'Took {time.time() - start:.2f}s')


if __name__ == '__main__':
    main()
