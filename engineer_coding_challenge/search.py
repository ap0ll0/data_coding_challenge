import random
import timeit


def search_range(array_a: list, target: int) -> int:
    left = 0
    right = len(array_a) - 1

    while left <= right:
        mid = left + (right - left) // 2

        if array_a[mid] < target:
            left = mid + 1
        elif array_a[mid] >= target:
            right = mid - 1

    if left < len(array_a) and array_a[left] >= target:
        return left
    else:
        return -1


def main() -> None:
    execution_times = []
    array_a_sizes = [int(x) for x in [1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7]]
    for size in array_a_sizes:
        nums = sorted(random.sample(range(size * 10), size))
        target = random.randint(0, size * 10)
        # Execute and time it.
        execution_time = timeit.timeit(lambda: search_range(nums, target))
        print(f'Input size={size} execution time={execution_time:2f} seconds')
        execution_times.append(execution_time)

    try:
        import matplotlib.pyplot as plt
        plt.plot(array_a_sizes, execution_times, marker='x')

        plt.xlabel('X (input array)')
        plt.ylabel('Y (execution time)')
        plt.title('Plot (scale: log n)')
        plt.xscale('log')

        plt.show()
    except ImportError as exc:
        print('please install matplotlib')
        raise exc

if __name__ == '__main__':
    main()