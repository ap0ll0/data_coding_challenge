try:
    from tqdm import tqdm
except ImportError:
    tqdm = lambda args: args  # noqa
import string


def create_random_datafile(fname: str, size: int = int(125e6 / 1e3)):
    with open(fname, 'w') as fhandle:
        for i in tqdm(range(0, size)):
            random_word = string.ascii_letters[i % 26] * 1000

            fhandle.write(f'line #{i}: {random_word}!\n')


if __name__ == '__main__':
    create_random_datafile('../input_file2.txt')
