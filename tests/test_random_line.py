import io
import pytest
import tempfile

from engineer_coding_challenge.random_line import IndexedReader


@pytest.fixture(scope="session")
def dataset_simple():
    yield ["apple", "pear", "orange", "lemon"]


@pytest.fixture(scope="session")
def get_dataset_f_handle(dataset_simple: list):
    memory_file = io.StringIO()
    memory_file.write("\n".join(dataset_simple) + "\n")
    memory_file.seek(0)
    yield memory_file

    memory_file.close()


@pytest.mark.parametrize("idx,expected", [(0, "apple"), (1, "pear"), (2, "orange"), (3, "lemon")])
def test_random_line_right_line_returned(idx: int, expected: str, get_dataset_f_handle, dataset_simple):
    """Ensure that random_line and its helper object return the right line/index for a test dataset."""
    helper = IndexedReader(f'{tempfile.gettempdir()}/{next(tempfile._get_candidate_names())}')  # noqa
    # Dataset handle is session-scoped fixture, so go to the beginning of the file each time.
    get_dataset_f_handle.seek(0)
    result = helper.get_random_line(get_dataset_f_handle, idx)

    assert result == expected
