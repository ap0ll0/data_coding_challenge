# Engineering Coding Challenge

**Author**: Jakub Racek <racek.j@seznam.cz>

# Requirements
* Python3.10+
* poetry
* Linux is preferred, Windows should also work

# Installation

```
pip install poetry
poetry install
```

## Running the project

After installing the project:

```
poetry run random-line input_file.txt 2286 
poetry run search
```

### Problem 1: Random line from a file
For validation, a small `input_file.txt` with line numbers is provided to see that lines are read correctly.

```
poetry run random-line input_file.txt 2286
line #2286: damn!
```

You can set `LOGURU_LEVEL=DEBUG` to print how long it took to find the line.

You can generate a ~125 MB file in `../` with 125000 lines immediately by using `create_random.py` script.

#### Implementation and further work

In my implementation, I relied on `.seek()` and building indexes to speed-up reading data.
The solution also uses "partial indexes", for example, if we have an index for `n`-th line and are looking
for `n`+1000th line, we can skip `n` lines.

The actual project requirements consider up to around 1TB file, which I couldn't create on my system, but could mock.
Without indexes, reading a 1TB mock-file in Python by 1000 character chunks took about 2 minutes, which seems OK, since basic operations
on an open file are already close to posix, system call interface and C code. It would require more research to optimize this.

The solution pre-builds indexes and considers up-to 8000 indexes for 1TB file; a regular interval of ~125MB of data. Indexes as a Python dictionary
like `{"n-th index": number_of_bytes_to_seek}"` for this should take around 200KB of disk space or less:

```
print(len("".join(f"{int(1e9)}: {interval*125e6}" for interval in range(0, int(1e12/125e6))))/1000, 'KB')
207.104 KB
```
After indexes are pre-built (~2 minutes), each search would take no longer than 6 seconds, even for 1TB file


### Problem 2: 

This takes about 30 seconds in total:

```
poetry run search
```

I chose binary search algorithm for problem 2. I had less time for this one, so I focused less on interface
and explaining the solution and more on presenting the validity. Array is already sorted, so we're just looking for
midpoint.
The script plots a graph using input size on X-axis and execution time
on Y-axis with log (n) scale, which draws a linear graph.

## Running tests

```
poetry run pytest tests
```

## If all else fails...

You really should use `poetry` to create virtual environment and avoid breaking your system-interpreter.
If, for whatever reason, you are unable to install poetry, you may run the individual Python files directly.
The scripts catch ImportErrors for non-standard libraries and will produce output.

Problem 1:
```
python engineer_coding_challenge\random_line.py input_file.txt 2286
line #2286: damn!
```

Problem 2:
```
python engineer_coding_challenge\search.py
Input size=10 execution time=0.541586 seconds
Input size=100 execution time=0.927968 seconds
Input size=1000 execution time=1.505814 seconds
Input size=10000 execution time=1.916166 seconds
Input size=100000 execution time=2.571182 seconds
Input size=1000000 execution time=2.909435 seconds
Input size=10000000 execution time=3.210767 seconds
```

This one needs `matplotlib`, so consider `pip install --user matplotlib`, so you can see the pretty graph.

## Possible improvements
* more tests
* more docstrings
* linters and other Python project niceties
* better graph for problem 2